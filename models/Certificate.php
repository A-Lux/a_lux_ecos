<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "certificate".
 *
 * @property int $id
 * @property string $image
 * @property string $image_en
 * @property string $image_kz
 */
class Certificate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/images/certificate/';
    public static function tableName()
    {
        return 'certificate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image', 'image_en', 'image_kz'], 'required'],
            [['image', 'image_en', 'image_kz'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'image_en' => 'Картинка (EN)',
            'image_kz' => 'Картинка (KZ)',
        ];
    }

    public function getImage($lang)
    {
        if($lang == '_en') $image = ($this->image_en) ? '/' . $this->path . $this->image_en : '/no-image.png';
        elseif ($lang == '_kz') $image = ($this->image_kz) ? '/' . $this->path . $this->image_kz : '/no-image.png';
        else $image = ($this->image) ? '/' . $this->path . $this->image : '/no-image.png';
        return $image;
    }

    public static function getAll(){
        return Certificate::find()->all();
    }
}
