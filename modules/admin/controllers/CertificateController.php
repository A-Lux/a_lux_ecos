<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Certificate;
use app\models\search\CertificateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CertificateController implements the CRUD actions for Certificate model.
 */
class CertificateController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Certificate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CertificateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Certificate model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Certificate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Certificate();
        if ($model->load(Yii::$app->request->post())) {

            $image = UploadedFile::getInstance($model, 'image');
            $imageEN = UploadedFile::getInstance($model, 'image_en');
            $imageKZ = UploadedFile::getInstance($model, 'image_kz');

            if($image != null) {
                $time = time();
                $image->saveAs($model->path . $time . '_ru_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_ru_' . $image->baseName . '.' . $image->extension;
            }

            if($imageEN != null) {
                $time = time();
                $imageEN->saveAs($model->path . $time . '_en_' . $imageEN->baseName . '.' . $imageEN->extension);
                $model->image_en = $time . '_en_' . $imageEN->baseName . '.' . $imageEN->extension;
            }

            if($imageKZ != null) {
                $time = time();
                $imageKZ->saveAs($model->path . $time . '_kz_' . $imageKZ->baseName . '.' . $imageKZ->extension);
                $model->image_kz = $time . '_kz_' . $imageKZ->baseName . '.' . $imageKZ->extension;
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Certificate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage = $model->image;
        $oldImageEN = $model->image_en;
        $oldImageKZ = $model->image_kz;

        if ($model->load(Yii::$app->request->post())) {

            $image = UploadedFile::getInstance($model, 'image');
            $imageEN = UploadedFile::getInstance($model, 'image_en');
            $imageKZ = UploadedFile::getInstance($model, 'image_kz');

            if($image == null){
                $model->image = $oldImage;
            }else{
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
                if($oldImage != null && file_exists($model->path . $oldImage)){
                    unlink($model->path . $oldImage);
                }
            }

            if($imageEN == null){
                $model->image_en = $oldImageEN;
            }else{
                $time = time();
                $imageEN->saveAs($model->path . $time . '_' . $imageEN->baseName . '.' . $imageEN->extension);
                $model->image_en = $time . '_' . $imageEN->baseName . '.' . $imageEN->extension;
                if($oldImageEN != null && file_exists($model->path . $oldImageEN)){
                    unlink($model->path . $oldImageEN);
                }
            }


            if($imageKZ == null){
                $model->image_kz = $oldImageKZ;
            }else{
                $time = time();
                $imageKZ->saveAs($model->path . $time . '_' . $imageKZ->baseName . '.' . $imageKZ->extension);
                $model->image_kz = $time . '_' . $imageKZ->baseName . '.' . $imageKZ->extension;
                if($oldImageKZ != null && file_exists($model->path . $oldImageKZ)){
                    unlink($model->path . $oldImageKZ);
                }
            }

            if($model->save()){
                return $this->redirect(['view', 'id' => $id]);
            }

        }


        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Certificate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Certificate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Certificate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Certificate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
