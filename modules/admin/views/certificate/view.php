<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Certificate */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Сертификаты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="certificate-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::a(Html::img($data->getImage(''), ['width'=>200]), [$data->getImage('')]);
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'image_en',
                'value' => function($data){
                    return Html::a(Html::img($data->getImage('_en'), ['width'=>200]), [$data->getImage('_en')]);
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'image_kz',
                'value' => function($data){
                    return Html::a(Html::img($data->getImage('_kz'), ['width'=>200]), [$data->getImage('_kz')]);
                }
            ],
        ],
    ]) ?>

</div>
