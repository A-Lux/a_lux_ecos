<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CertificateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Сертификаты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="certificate-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::a(Html::img($data->getImage(''), ['width'=>100]), [$data->getImage('')]);
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'image_en',
                'value' => function($data){
                    return Html::a(Html::img($data->getImage('_en'), ['width'=>100]), [$data->getImage('_en')]);
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'image_kz',
                'value' => function($data){
                    return Html::a(Html::img($data->getImage('_kz'), ['width'=>100]), [$data->getImage('_kz')]);
                }
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
