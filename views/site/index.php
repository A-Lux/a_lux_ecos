<div class="border mt-4"></div>
<div class="container">
<div class="row">
<div class="col-sm-12 col-md">
<nav class="menu" id="main-nav">
<ul>
<? foreach (Yii::$app->view->params['headerMenu'] as $v):?>
<li class="<?=$v->url == "/"?"acting":"";?>"><a href="<?=$v->url;?>"><?=$v->getName();?></a></li>
<? endforeach;?>
</ul>
</nav>
</div>
</div>
</div>
<div class="container-fluid">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="4000">
<div class="carousel-inner">
<? $class = 'active';?>
<? foreach ($banner as $v):?>
<div class="carousel-item <?=$class;?> bg-01" style="background-image: url(<?=$v->getImage();?>);">
<div class="d-block">
<div class="slide-01 ml-4">
<h1>
<?=$v->title;?> <span class="mt-5"><?=$v->subTitle;?></span>
</h1>
</div>
</div>
</div>
<? $class = "";?>
<? endforeach;?>
</div>
<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
<span class="carousel-control-prev-icon" aria-hidden="true"></span>
<span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
<span class="carousel-control-next-icon" aria-hidden="true"></span>
<span class="sr-only">Next</span>
</a>
</div>
</div>
<div class="container service">
<div class="row">
<div class="col-12">
<h2 class="ts mb-4"><?=Yii::$app->view->params['translation'][22]->text;?></h2>
</div>
</div>
<? $m = 0;?>
<? foreach ($services as $v):?>
<? $m++;?>
<? if($m % 4 == 1):?>
<div class="row">
<? endif;?>
<? $col = 'name'.Yii::$app->session["lang"];?>
<div class="col-sm-12 col-md-3">
<a href="/service?id=<?=$v->id;?>"><span><img class="img-effect <?=$v->class;?>"><p><?=$v->$col;?></p></span></a>
</div>
<? if($m % 4 == 0):?>
</div>
<? endif;?>
<? endforeach;?>
<? if($m % 4 != 0):?>
</div>
<? endif;?>
</div>
<? if(!isset(Yii::$app->session["lang"])):?>
<div class="modal fade" id="chooseLang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-12">
<h3>Для перехода на сайт ecos.kz выберите язык</h3>
</div>
</div>
</div>
<a class="close" data-dismiss="modal">×</a>
</div>
<div class="modal-body">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-12">
<ul class="chooseLanguageClass">
<li><a href="/lang?url=kz">Казахский</a></li>
<li><a href="/lang?url=ru">Русский</a></li>
<li><a href="/lang?url=en">Английский</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="border mt-4"></div>

<script type="text/javascript">
$(window).on('load',function(){
$('#chooseLang').modal('show');
});
</script>
<? endif;?>

